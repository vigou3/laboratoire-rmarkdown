%%% Copyright (C) 2024 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet «Programmation lettrée et R
%%% Markdown»
%%% https://gitlab.com/vigou3/programmation-lettree-et-rmarkdown
%%%
%%% Cette création est mise à disposition sous licence
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.

\documentclass[aspectratio=169,10pt,xcolor=x11names,french]{beamer}
  \usepackage{babel}
  \usepackage[babel=true]{microtype}
  \usepackage[autolanguage]{numprint}
  \usepackage[mathrm=sym]{unicode-math}  % polices math
  \usepackage{fontawesome5}              % various icons
  \usepackage{awesomebox}                % \tipbox et autres
  \usepackage{upquote}                   % accents droits dans code
  \usepackage{relsize}
  \usepackage{fancyvrb}                  % texte verbatim
  \usepackage[overlay,absolute]{textpos} % couvertures
  \usepackage{metalogo}                  % logo \XeLaTeX

  %%% =============================
  %%%  Informations de publication
  %%% =============================
  \title{Programmation lettrée et R~Markdown}
  \author{Introduction}
  \date{}

  %%% ===================
  %%%  Style du document
  %%% ===================

  %% Thème Beamer. Personnalisation pour le tableau lumineux.
  \usetheme[background=dark,numbering=none]{metropolis}
  \setbeamercolor{normal text}{fg=white, bg=black}
  \setbeamercolor{frametitle}{fg=alert, bg=black}
  \setbeamersize{text margin right=90mm}

  %% Polices de caractères
  \setsansfont{Fira Sans Book}
  [
    BoldFont = {Fira Sans SemiBold},
    ItalicFont = {Fira Sans Book Italic},
    BoldItalicFont = {Fira Sans SemiBold Italic}
  ]

  %% Couleurs
  \colorlet{alert}{mLightBrown}
  \colorlet{code}{mLightGreen}

  %% Paramétrage de babel pour les guillemets
  \frenchbsetup{og=«, fg=»}

  %% =====================
  %%  Nouvelles commandes
  %% =====================

  %% Noms de fonctions, code, environnement, etc.
  \newcommand{\code}[1]{\textcolor{code}{\texttt{#1}}}

\begin{document}

%% frontmatter
\maketitle

%% mainmatter
\begin{frame}
  \centering
  \larger[3]
  \mbox{R Markdown}
\end{frame}

\begin{frame}
  \frametitle{Recherche reproductible}

  \begin{itemize}
  \item Fichiers en format facile à sauvegarder, à modifier et à
    partager
  \item Générer les rapports à partir du code informatique
  \item Tout conserver dans un système de gestion de versions
  \end{itemize}
\end{frame}

\begin{frame}
  \begin{textblock*}{\paperwidth}(0mm,0mm)
    \includegraphics[height=\paperheight]{Donald-Knuth_2880_Lede}
  \end{textblock*}
\end{frame}

\begin{frame}
  \frametitle{Programmation lettrée}

  Plusieurs systèmes existent:
  \bigskip

  WEB \\
  CWEB \\
  noweb \\
  Sweave \\
  knitr \\
  \dots
\end{frame}

\begin{frame}
  \frametitle{Sweave $+$ \LaTeX}

  Texte en format {\LaTeX} avec code informatique R
  \bigskip

  \pause
  \begin{itemize}
  \item[$+$] Flexibilité et puissance
  \item[$-$] Courbe d'apprentissage
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{R~Markdown}

  Texte en format Markdown avec code informatique R
  \bigskip

  \pause
  \begin{itemize}
  \item[$+$] Prise en main rapide et facile
  \item[$-$] Nombreuses couches logicielles
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Markdown}

  \begin{itemize}
  \item Langage de balisage léger devenu un standard \emph{de facto}
    dans les sites web modernes
  \item Simple --- pour la mise en forme simple
  \end{itemize}
\end{frame}

\begin{frame}[fragile]

\begin{Verbatim}[fontsize=\footnotesize]
# Syntaxe Markdown

Markdown permet d'indiquer
simplement de l'*italique*
ou du **gras**.

## Listes

- Épicerie
  1. Bananes
  2. Hamburger
- Quincaillerie
\end{Verbatim}

  \begin{textblock*}{0.3\paperwidth}(105mm,20mm)
    \includegraphics[width=\linewidth]{markdown-preview}
  \end{textblock*}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{R Markdown}

\begin{Verbatim}[fontsize=\small]
# Exemple simple

L'utilisateur de R interagit
avec l'interpréteur en entrant
des commandes à l'invite de
commande:
```{r echo=TRUE}
2 + 3
```

La commande `exp(1)` donne
`r exp(1)`, la valeur du
nombre $e$.
\end{Verbatim}
\end{frame}

\begin{frame}
  \frametitle{Fonctionnement}

  \begin{textblock*}{150mm}(10mm,45mm)
    \onslide<1->{%
      \begin{minipage}[t]{12mm}
        \centering
        {\huge\faFile*[regular]} \\
        \bigskip
        \footnotesize\code{foo.Rmd}
      \end{minipage}}
    \onslide<2->{%
      \hfill{\Large\faArrowRight}\hfill
      \begin{minipage}[t]{12mm}
        \centering
        {\huge\faCogs} \\
        \bigskip
        \footnotesize\code{knitr}
      \end{minipage}}
    \onslide<3->{%
      \hfill{\Large\faArrowRight}\hfill
      \begin{minipage}[t]{12mm}
        \centering
        {\huge\faFile*[regular]} \\
        \bigskip
        \footnotesize\code{foo.md}
      \end{minipage}}
    \onslide<4->{%
      \hfill{\Large\faArrowRight}\hfill
      \begin{minipage}[t]{15mm}
        \centering
        {\huge\faCogs} \\
        \bigskip
        \footnotesize\code{pandoc}
      \end{minipage}}
    \onslide<5->{%
      \hfill
      \parbox{5mm}{
        \rotatebox{45}{\Large\faArrowRight} \\
        \rotatebox{-45}{\Large\faArrowRight}}
      \hfill
      \begin{minipage}{65mm}
        \begin{minipage}[t]{10mm}
        \centering
        {\LARGE\faFile*[regular]} \\
        \medskip
        \footnotesize\code{foo.tex}
      \end{minipage}
      \;{\large\faArrowRight}\;
      \begin{minipage}[t]{10mm}
        \centering
        {\LARGE\faCogs} \\
        \medskip
        \footnotesize\code{latex}
      \end{minipage}
      \;{\large\faArrowRight}\;
      \begin{minipage}[t]{10mm}
        \centering
        {\LARGE\faFilePdf[regular]} \\
        \medskip
        \footnotesize\code{foo.pdf}
      \end{minipage} \\
      \phantom{\LARGE\faFile*[regular]} \\
      \begin{minipage}[t]{10mm}
        \centering
        {\LARGE\faFileCode[regular]} \\
        \medskip
        \footnotesize\code{foo.html}
      \end{minipage}
      \;{\large\faArrowRight}\;
      \begin{minipage}[t]{10mm}
        \centering
        {\LARGE\faGlobe} \\
        \medskip
        \footnotesize Web
      \end{minipage}
      \hspace*{\fill}
    \end{minipage}}
  \end{textblock*}
\end{frame}

\begin{frame}
\end{frame}

%% backmatter
\begin{frame}[plain]
  \smaller[3] %
  Ce document a été produit par le système de mise en page {\XeLaTeX}
  avec la classe \textbf{beamer} et le thème Metropolis. Les titres et
  le texte sont composés en Fira~Sans, le code informatique en
  Fira~Mono. Les icônes proviennent de la police Font~Awesome. Les
  illustrations ont été entièrement réalisées avec {\LaTeX}.
\end{frame}

\end{document}

%%% Local Variables:
%%% TeX-engine: xetex
%%% TeX-master: t
%%% coding: utf-8
%%% End:
